#include <iostream>
#include <ctime>

const int arraySize = 10;

using namespace std;

/**
 * Found on Stackoverflow
 *
 * @return Current day of month
 */
int getCurrentDayOfMonth()
{
    time_t theTime = time(NULL);
    struct tm *aTime = localtime(&theTime);

    return aTime->tm_mday;
}

int main() {
    int hwArray[arraySize][arraySize];
    int currentDay = getCurrentDayOfMonth();
    int rowToCalculate = currentDay % arraySize;
    int rowSum = 0;

    for (int i = 0; i < arraySize; i++) {
        for (int j = 0; j < arraySize; j++) {
            int value = i + j;
            hwArray[i][j] = value;
            cout << value<< '\t';

            if (j == rowToCalculate) {
                rowSum += value;
            }
        }
        cout << endl;
    }
    cout << "Sum of " << rowToCalculate << " row is " << rowSum << endl;

}
